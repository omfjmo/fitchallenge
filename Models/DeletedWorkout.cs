﻿using System;
using System.Collections.Generic;

namespace FitChallenge.Models;

public partial class DeletedWorkout
{
    public int Id { get; set; }

    public string Description { get; set; } = null!;

    public DateTime DateTime { get; set; }

    public int Duration { get; set; }

    public int MandatorySportId { get; set; }

    public int CategoryId { get; set; }

    public DateTime DeleteTime { get; set; }

    public virtual Category Category { get; set; } = null!;

    public virtual MandatorySport MandatorySport { get; set; } = null!;

    public DeletedWorkout()
    {

    }
    public DeletedWorkout(string description, DateTime dateTime, int duration, int mandatorySportId, int categoryId)
    {
        Description = description;
        DateTime = dateTime;
        Duration = duration;
        MandatorySportId = mandatorySportId;
        CategoryId = categoryId;
        DeleteTime = DateTime.Now;
    }
}
