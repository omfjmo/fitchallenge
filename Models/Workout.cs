﻿using Microsoft.Build.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FitChallenge.Models;

public partial class Workout
{
    public int Id { get; set; }

    public string Description { get; set; } = null!;

    public DateTime DateTime { get; set; }

    [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than zero")]
    public int Duration { get; set; }

    public int MandatorySportId { get; set; }

    public int CategoryId { get; set; }

    public virtual Category Category { get; set; } = null!;

    public virtual MandatorySport MandatorySport { get; set; } = null!;

    public Workout()
    {

    }
    public Workout(string description, DateTime dateTime, int duration, int mandatorySportId, int categoryId)
    {
        Description = description;
        DateTime = dateTime;
        Duration = duration;
        MandatorySportId = mandatorySportId;
        CategoryId = categoryId;
    }
}
