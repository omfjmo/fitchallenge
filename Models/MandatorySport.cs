﻿using System;
using System.Collections.Generic;

namespace FitChallenge.Models;

public partial class MandatorySport
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public virtual ICollection<DeletedWorkout> DeletedWorkouts { get; } = new List<DeletedWorkout>();

    public virtual ICollection<Workout> Workouts { get; } = new List<Workout>();
}
