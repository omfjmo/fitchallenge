﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace FitChallenge.Models;

public partial class Workouts : DbContext
{
    public Workouts()
    {
    }

    public Workouts(DbContextOptions<Workouts> options)
        : base(options)
    {
    }

    public virtual DbSet<Category> Categories { get; set; }

    public virtual DbSet<DeletedWorkout> DeletedWorkouts { get; set; }

    public virtual DbSet<MandatorySport> MandatorySports { get; set; }

    public virtual DbSet<Workout> Trainings { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Brigi\\source\\repos\\FitChallenge\\workouts.mdf;Integrated Security=True;Connect Timeout=30");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Category>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Category__3214EC07C401645C");

            entity.ToTable("Category");

            entity.Property(e => e.Name)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        modelBuilder.Entity<DeletedWorkout>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__DeletedW__3214EC07116C117A");

            entity.ToTable("DeletedWorkout");

            entity.Property(e => e.CategoryId).HasColumnName("Category_id");
            entity.Property(e => e.DateTime)
                .HasColumnType("datetime")
                .HasColumnName("Date_time");
            entity.Property(e => e.DeleteTime)
                .HasDefaultValueSql("(((2022)-(11))-(26))")
                .HasColumnType("datetime")
                .HasColumnName("Delete_time");
            entity.Property(e => e.Description)
                .HasMaxLength(200)
                .IsUnicode(false);
            entity.Property(e => e.MandatorySportId).HasColumnName("Mandatory_sport_id");

            entity.HasOne(d => d.Category).WithMany(p => p.DeletedWorkouts)
                .HasForeignKey(d => d.CategoryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("foreign_category_id");

            entity.HasOne(d => d.MandatorySport).WithMany(p => p.DeletedWorkouts)
                .HasForeignKey(d => d.MandatorySportId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("foreign_mandatory_sport_id");
        });

        modelBuilder.Entity<MandatorySport>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Mandator__3214EC0720A7D7B9");

            entity.ToTable("Mandatory_Sports");

            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Workout>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Workout__3214EC0700A7A135");

            entity.ToTable("Workout");

            entity.Property(e => e.CategoryId).HasColumnName("Category_id");
            entity.Property(e => e.DateTime)
                .HasColumnType("datetime")
                .HasColumnName("Date_time");
            entity.Property(e => e.Description)
                .HasMaxLength(200)
                .IsUnicode(false);
            entity.Property(e => e.MandatorySportId).HasColumnName("Mandatory_sport_id");

            entity.HasOne(d => d.Category).WithMany(p => p.Workouts)
                .HasForeignKey(d => d.CategoryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("idegen_category_id");

            entity.HasOne(d => d.MandatorySport).WithMany(p => p.Workouts)
                .HasForeignKey(d => d.MandatorySportId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("idegen_mandatory_sport_id");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
