﻿using FitChallenge.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace FitChallenge.Controllers
{
    public class WorkoutsController : Controller
    {
        private Workouts _dbContext;
        public WorkoutsController(Workouts context)
        {
            _dbContext = context;
        }
        public async Task<IActionResult> Index()
        {
            var workouts = await _dbContext.Trainings
                .Include(w => w.Category)
                .Include(w => w.MandatorySport)
                .OrderBy(w => w.DateTime).ToListAsync();

            return View(workouts);
        }

        public IActionResult Create()
        {
            ViewData["CategoryId"] = new SelectList(_dbContext.Categories, "Id", "Name");
            ViewData["MandatorySportId"] = new SelectList(_dbContext.MandatorySports, "Id", "Name");
           
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create([Bind("Id,Description,DateTime,Duration,MandatorySportId,CategoryId")] Workout workout)
        {
            if (workout != null)
            {
                try
                {
                    _dbContext.Trainings.Add(workout);
                    await _dbContext.SaveChangesAsync();
                    return Ok("Sikeres hozzáadás!");
                }
                catch (DbUpdateException)
                {
                    return NotFound("Sikertelen hozzáadás!");
                }
            }

            ViewData["CategoryId"] = new SelectList(_dbContext.Categories, "Id", "Name", workout.Category.Name);
            ViewData["MandatorySportId"] = new SelectList(_dbContext.MandatorySports, "Id", "Name", workout.MandatorySport.Name);
            
            return View(workout);
        }
        
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _dbContext.Trainings == null)
            {
                return NotFound();
            }

            var workout = await _dbContext.Trainings.FindAsync(id);

            if (workout == null)
            {
                return NotFound("A keresett id nem található!");
            }

            ViewData["CategoryId"] = new SelectList(_dbContext.Categories, "Id", "Name");
            ViewData["MandatorySportId"] = new SelectList(_dbContext.MandatorySports, "Id", "Name");
            
            return View(workout);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description,DateTime,Duration,MandatorySportId,CategoryId")] Workout workout)
        {
            if (id != workout.Id)
            {
                return NotFound();
            }

            if (workout != null)
            {
                try
                {
                    _dbContext.Update(workout);
                    await _dbContext.SaveChangesAsync();
                    return Ok("Sikeres módosítás!");
                }
                catch (DbUpdateException)
                {
                    return NotFound("Sikertelen módosítás!");
                }
            }

            ViewData["CategoryId"] = new SelectList(_dbContext.Categories, "Id", "Name", workout.Category.Name);
            ViewData["MandatorySportId"] = new SelectList(_dbContext.MandatorySports, "Id", "Name", workout.MandatorySport.Name);
            
            return View(workout);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _dbContext.Trainings == null)
            {
                return NotFound();
            }

            var workout = await _dbContext.Trainings
                .Include(w => w.Category)
                .Include(w => w.MandatorySport)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (workout == null)
            {
                return NotFound("A keresett id nem található!");
            }

            return View(workout);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            if (_dbContext.Trainings == null)
            {
                return Problem("'Workouts.Trainings' értéke null.");
            }

            var workout = _dbContext.Trainings.Find(id);

            if (workout != null)
            {
                try
                {
                    _dbContext.DeletedWorkouts.Add
                        (new DeletedWorkout(workout.Description,workout.DateTime,
                        workout.Duration, workout.MandatorySportId,workout.CategoryId));
                    await _dbContext.SaveChangesAsync();

                    _dbContext.Trainings.Remove(workout);
                    await _dbContext.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    return NotFound("Sikertelen törlés!");
                }
            }
            
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> PlannedWorkouts()
        {
            var workouts = await _dbContext.Trainings
                .Include(w => w.Category)
                .Include(w => w.MandatorySport)
                .Where(w => w.DateTime > DateTime.Now)
                .OrderBy(w => w.DateTime).ToListAsync();

            return View(workouts);
        }

        public async Task<IActionResult> DailyWorkouts()
        {
            var dailyWorkouts = await _dbContext.Trainings
                .Include(w => w.Category)
                .Include(w => w.MandatorySport)
                .Where(w => w.DateTime.Date == DateTime.Today && w.DateTime > DateTime.Now)
                .OrderBy(w => w.DateTime).ToListAsync();

            return View(dailyWorkouts);
        }

        public async Task<IActionResult> WeeklyWorkouts()
        {
            var now = DateTime.Now;
            var currentDay = now.DayOfWeek;
            int days = (int)currentDay;
            DateTime sunday = now.AddDays(-days);
            List<DateTime> myDaysOfWeek = Enumerable.Range(0, 7)
                .Select(d => sunday.AddDays(d))
                .ToList();

            var startDay = myDaysOfWeek.First();
            var endDay = myDaysOfWeek.Last();

            var weeklyWorkouts = await _dbContext.Trainings
                .Include(w => w.Category)
                .Include(w => w.MandatorySport)
                .Where(w => w.DateTime.Date >= startDay && w.DateTime.Date <= endDay
                 && w.DateTime.AddMinutes(w.Duration) < DateTime.Now)
                .OrderBy(w => w.DateTime).ToListAsync();

            return View(weeklyWorkouts);
        }

        public async Task<IActionResult> AllActiveWorkouts()
        {
            var allActiveWorkouts = await _dbContext.Trainings
                .Include(w => w.Category)
                .Include(w => w.MandatorySport)
                .Where(w => w.DateTime < DateTime.Now && w.DateTime.AddMinutes(w.Duration) > DateTime.Now)
                .OrderBy(w => w.DateTime).ToListAsync();

            return View(allActiveWorkouts);
        }
        public async Task<IActionResult> DeletedWorkouts()
        {
            var workouts = await _dbContext.DeletedWorkouts
                .Include(w => w.Category)
                .Include(w => w.MandatorySport)
                .OrderBy(w => w.DateTime).ToListAsync();

            return View(workouts);
        }
        public async Task<IActionResult> Restore(int id)
        {
            var workout = _dbContext.DeletedWorkouts.Find(id);

            if (workout != null)
            {
                try
                {
                    _dbContext.Trainings.Add(new Workout(workout.Description, workout.DateTime,
                        workout.Duration, workout.MandatorySportId, workout.CategoryId));
                    await _dbContext.SaveChangesAsync();

                    _dbContext.DeletedWorkouts.Remove(workout);
                    await _dbContext.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    return NotFound("Sikertelen visszaállítás!");
                }
            }

            return RedirectToAction("Index");
        }
    }
}
