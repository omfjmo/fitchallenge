﻿using FitChallenge.Models;
using Microsoft.AspNetCore.Mvc;

namespace FitChallenge.Controllers
{
    public class DiagramsController : Controller
    {
        private Workouts _dbContext;
        public DiagramsController(Workouts context)
        {
            _dbContext = context;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult DrawPieChart()
        {
            Dictionary<int, int> workouts = new();
            foreach (Workout w in _dbContext.Trainings)
            {
                if (workouts.ContainsKey(w.CategoryId))
                {
                    workouts[w.CategoryId] += 1;
                }
                else
                {
                    workouts.Add(w.CategoryId, 1);
                }    
            }
            
            return View(workouts);
        }

        public IActionResult DrawLineChart()
        {
            var workouts = _dbContext.Trainings
                 .Select(w => w)
                 .Where(w => w.DateTime < DateTime.Now);

            Dictionary<string, int> sports = new();
            foreach (var w in workouts)
            {
                if (sports.ContainsKey(w.Description))
                {
                    sports[w.Description] += 1;
                }
                else
                {
                    sports.Add(w.Description, 1);
                }
            }

            return View(sports);
        }
    }
}
